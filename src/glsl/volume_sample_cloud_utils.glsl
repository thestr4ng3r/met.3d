/******************************************************************************
**
**  This file is part of Met.3D -- a research environment for the
**  three-dimensional visual exploration of numerical ensemble weather
**  prediction data.
**
**  Copyright 2018 Florian Märkl
**
**  Computer Graphics and Visualization Group
**  Technische Universitaet Muenchen, Garching, Germany
**
**  Met.3D is free software: you can redistribute it and/or modify
**  it under the terms of the GNU General Public License as published by
**  the Free Software Foundation, either version 3 of the License, or
**  (at your option) any later version.
**
**  Met.3D is distributed in the hope that it will be useful,
**  but WITHOUT ANY WARRANTY; without even the implied warranty of
**  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
**  GNU General Public License for more details.
**
**  You should have received a copy of the GNU General Public License
**  along with Met.3D.  If not, see <http://www.gnu.org/licenses/>.
**
******************************************************************************/

// requires volume_global_structs_utils.glsl
// requires volume_<datatype>_utils.glsl
// requires volume_sample_utils.glsl

uniform DataVolumeExtent dataExtentLWC;
uniform DataVolumeExtent dataExtentIWC;

uniform sampler1D pressureTableLWC; // PRESSURE_LEVEL
uniform sampler1D pressureTableIWC; // PRESSURE_LEVEL

uniform sampler1D hybridCoefficientsLWC; // HYBRID_SIGMA
uniform sampler1D hybridCoefficientsIWC; // HYBRID_SIGMA

uniform sampler2D surfacePressureLWC; // HYBRID_SIGMA
uniform sampler2D surfacePressureIWC; // HYBRID_SIGMA

#ifdef ENABLE_HYBRID_PRESSURETEXCOORDTABLE
// contains precomputed z-coordinates
uniform sampler2D pressureTexCoordTable2DLWC; // HYBRID_SIGMA
uniform sampler2D pressureTexCoordTable2DIWC; // HYBRID_SIGMA
#endif

#ifdef ENABLE_MINMAX_ACCELERATION
// Regular grid that uniformly divides space and stores min/max for each voxel.
// Is used to skip regions in which an isosurface cannot be located. See
// MStructuredGrid::getMinMaxAccelTexture3D().
uniform sampler3D minMaxAccel3DLWC;
uniform sampler3D minMaxAccel3DIWC;
#endif

uniform sampler1D lonLatLevAxesLWC;
uniform sampler1D lonLatLevAxesIWC;

vec2 sampleCloudWaterContentAtPos(in vec3 pos)
{
    float lwc = 0.0;
    // case PRESSURE_LEVEL_3D
    if (dataExtentLWC.levelType == 0)
    {
        lwc = samplePressureLevelVolumeAtPos(dataVolumeLWC, dataExtentLWC,
                                             pressureTableLWC, pos);
    }
    // case HYBRID_SIGMA_PRESSURE_3D
    else if (dataExtentLWC.levelType == 1)
    {
        lwc = sampleHybridSigmaVolumeAtPos(dataVolumeLWC, dataExtentLWC,
                                           surfacePressureLWC, hybridCoefficientsLWC,
                                           pos);
    }

    if(lwc < 0.0)
    {
        lwc = 0.0;
    }


    float iwc = 0.0;
    // case PRESSURE_LEVEL_3D
    if (dataExtentIWC.levelType == 0)
    {
        iwc = samplePressureLevelVolumeAtPos(dataVolumeIWC, dataExtentIWC,
                                             pressureTableIWC, pos);
    }
    // case HYBRID_SIGMA_PRESSURE_3D
    else if (dataExtentIWC.levelType == 1)
    {
        iwc = sampleHybridSigmaVolumeAtPos(dataVolumeIWC, dataExtentIWC,
                                           surfacePressureIWC, hybridCoefficientsIWC,
                                           pos);
    }

    if(iwc == M_MISSING_VALUE)
    {
        iwc = 0.0;
    }

    return vec2(lwc, iwc);
}