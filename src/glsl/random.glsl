/******************************************************************************
**
**  This file is part of Met.3D -- a research environment for the
**  three-dimensional visual exploration of numerical ensemble weather
**  prediction data.
**
**  Copyright 2017 Marc Rautenhaus
**  Copyright 2017 Florian Maerkl
**
**  Computer Graphics and Visualization Group
**  Technische Universitaet Muenchen, Garching, Germany
**
**  Met.3D is free software: you can redistribute it and/or modify
**  it under the terms of the GNU General Public License as published by
**  the Free Software Foundation, either version 3 of the License, or
**  (at your option) any later version.
**
**  Met.3D is distributed in the hope that it will be useful,
**  but WITHOUT ANY WARRANTY; without even the implied warranty of
**  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
**  GNU General Public License for more details.
**
**  You should have received a copy of the GNU General Public License
**  along with Met.3D.  If not, see <http://www.gnu.org/licenses/>.
**
******************************************************************************/


/*
 * Pseudo Random Number Generator
 *
 * Implemented after GPU Gems 3, Chapter 37. Efficient Random Number Generation
 * and Application Using CUDA
 * see https://developer.nvidia.com/gpugems/GPUGems3/gpugems3_ch37.html
 */

uint rand_z1, rand_z2, rand_z3, rand_z4;

uint randTausStep(inout uint z, int S1, int S2, int S3, uint M)
{
	uint b = ((z << S1) ^ z) >> S2;
	return z = ((z & M) << S3) ^ b;
}

uint randLCGStep(inout uint z, uint A, uint C)
{
	return z = A*z + C;
}

float randHybridTaus()
{
	return 2.3283064365387e-10 * float(
		randTausStep(rand_z1, 13, 19, 12, 4294967294U) ^
		randTausStep(rand_z2, 2, 25, 4, 4294967288U) ^
		randTausStep(rand_z3, 3, 11, 17, 4294967280U) ^
		randLCGStep(rand_z4, 1664525, 1013904223U)
	);
}

vec2 randHybridTaus2()
{
	return vec2(randHybridTaus(), randHybridTaus());
}

vec3 randHybridTaus3()
{
	return vec3(randHybridTaus(), randHybridTaus(), randHybridTaus());
}

vec4 randHybridTaus4()
{
	return vec4(randHybridTaus(), randHybridTaus(), randHybridTaus(), randHybridTaus());
}

void randSetSeed(uint seed)
{
	rand_z1 = seed * 1099087573U;
	rand_z2 = rand_z1 * 1099087573U;
	rand_z3 = rand_z2 * 1099087573U;
	rand_z4 = rand_z3 * 1099087573U;
}
