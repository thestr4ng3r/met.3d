/******************************************************************************
**
**  This file is part of Met.3D -- a research environment for the
**  three-dimensional visual exploration of numerical ensemble weather
**  prediction data.
**
**  Copyright 2017 Marc Rautenhaus
**  Copyright 2017 Florian Maerkl
**
**  Computer Graphics and Visualization Group
**  Technische Universitaet Muenchen, Garching, Germany
**
**  Met.3D is free software: you can redistribute it and/or modify
**  it under the terms of the GNU General Public License as published by
**  the Free Software Foundation, either version 3 of the License, or
**  (at your option) any later version.
**
**  Met.3D is distributed in the hope that it will be useful,
**  but WITHOUT ANY WARRANTY; without even the implied warranty of
**  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
**  GNU General Public License for more details.
**
**  You should have received a copy of the GNU General Public License
**  along with Met.3D.  If not, see <http://www.gnu.org/licenses/>.
**
******************************************************************************/

/*****************************************************************************
 ***                             CONSTANTS
 *****************************************************************************/

#define M_PI 3.1415926535897932384626433832795

const float PHOTON_MAP_NORMALIZATION_VALUE = 128.0;

const float PHOTON_FLUX_THRESHOLD = 0.01;

const int MAX_ISOSURFACES = 10;


/*****************************************************************************
 ***                             UNIFORMS
 *****************************************************************************/

// textures
// ========
uniform sampler1D transferFunction;

// contains precomputed z-coordinates
uniform sampler1D pressureTable; // PRESSURE_LEVEL
// contains hybrid coefficients a,b
uniform sampler1D hybridCoefficients; // HYBRID_SIGMA
// contains surface pressure at grid point (i, j)
uniform sampler2D surfacePressure; // HYBRID_SIGMA
// contains pressure field
uniform sampler3D auxPressureField3D_hPa; // AUXILIARY_PRESSURE_3D
uniform sampler3D auxPressureField3DShV_hPa; // AUXILIARY_PRESSURE_3D


uniform sampler3D dataVolume;
uniform sampler3D dataVolumeLWC; // liquid water content
uniform sampler3D dataVolumeIWC; // ice water content
uniform sampler1D lonLatLevAxes;

// photon lighting maps
uniform usampler3D photonFluxMap;
uniform isampler3D photonCosineMap;
uniform usampler3D photonCountMap;

// matrices
// ========
uniform mat4    mvpMatrix;

// vectors
// =======
uniform vec3    cameraPosition;
uniform vec3    volumeBottomSECrnr;
uniform vec3    volumeTopNWCrnr;

// scalars
// =======
uniform uint    bisectionSteps;

// Multi-isosurface settings
// =========================
uniform float   isoValues[MAX_ISOSURFACES];
uniform int     numIsoValues;

uniform vec2    pToWorldZParams;

// Volume lighting Settings
// ========================
uniform vec3	volumeLightingDirection;
uniform float   volumeLightingDensityScale;
uniform float   volumeLightingMaxExtinctionCoeff;
uniform float   volumeLightingHenyeyGreensteinG;

// Mode
// ====
uniform uint    variableMode;

// pre-computed probabilities for a photon to fall on dataExtent planes with
// normal (1,0,0) and (0,1,0) when shot from volumeLightingDirection
uniform vec2	photonPlaneProbabilities;




layout(r32ui, binding = 0) uniform uimage3D photonFluxMapOut;
layout(r32i, binding = 1)  uniform iimage3D photonCosineMapOut;
layout(r32ui, binding = 2) uniform uimage3D photonCountMapOut;

layout(rg32f, binding = 1) uniform image3D photonCacheCompleteOut;

layout(r32f, binding = 0) uniform image3D lightingMapOut;


/*****************************************************************************
 ***                             INCLUDES
 *****************************************************************************/

// include global structs
#include "volume_global_structs_utils.glsl"
// include hybrid model volume sampling methods
#include "volume_hybrid_utils.glsl"
// include model level volume with auxiliary pressure field sampling methods
#include "volume_auxiliarypressure_utils.glsl"
// include pressure levels volume sampling methods
#include "volume_pressure_utils.glsl"
// defines subroutines and auxiliary ray-casting functions
#include "volume_sample_utils.glsl"
// shading variable sampling methods
#include "volume_sample_cloud_utils.glsl"
// include lighting volume sampling methods
#include "volume_lighting_utils.glsl"

#include "random.glsl"

/*****************************************************************************
 ***                          COMPUTE SHADER
 *****************************************************************************/

shader CSSimpleLighting()
{
	ivec3 size = imageSize(lightingMapOut);
	vec3 boundsMin = getLightingWorldBoundsMin(dataExtent);
	vec3 boundsMax = getLightingWorldBoundsMax(dataExtent);
	float stepSizeWorld = (boundsMax.z - boundsMin.z) / float(size.z);
	float stepSizeMeters = worldZToMeters(stepSizeWorld);

	ivec3 texelCoord = ivec3(gl_GlobalInvocationID.xy, 0);
	float currentOpticalThickness = 0.0;

	while(texelCoord.z < size.z)
	{
		vec3 texCoord = vec3(texelCoord) / vec3(size);
		vec3 worldPos = getLightingWorldPositionFromBounds(boundsMin, boundsMax, texCoord);

		float extCoeff = sampleExtinctionCoefficient(worldPos);
		currentOpticalThickness += opticalThickness(stepSizeMeters, extCoeff);

		imageStore(lightingMapOut, texelCoord, vec4(currentOpticalThickness, vec3(0.0)));

		texelCoord.z++;
	}
}

/*****************************************************************************/

vec3 randomPhotonEntry()
{
	vec2 flatCoord = randHybridTaus2();
	vec3 texCoord;

	float areaSelect = randHybridTaus();
	if(areaSelect < photonPlaneProbabilities.x)
	{
		texCoord = vec3(0.0, flatCoord.x, flatCoord.y);
		if(volumeLightingDirection.x < 0.0)
		{
			texCoord.x = 1.0;
		}
	}
	else if(areaSelect < photonPlaneProbabilities.y)
	{
		texCoord = vec3(flatCoord.x, 0.0, flatCoord.y);
		if(volumeLightingDirection.y > 0.0)
		{
			texCoord.y = 1.0;
		}
	}
	else
	{
		texCoord = vec3(flatCoord.x, flatCoord.y, 0.0);

		if (earthCosineEnabled)
		{
            while(randHybridTaus() >
                cos(getLightingWorldPosition(dataExtent, texCoord).y * M_PI / 180.0))
            {
                texCoord.y = randHybridTaus();
            }
		}


		if(volumeLightingDirection.y > 0.0)
		{
			texCoord.z = 1.0;
		}
	}

	return getLightingWorldPosition(dataExtent, texCoord);
}

/**
 * returns a rotation matrix orienting z along zDir
 * zDir must be normalized.
 */
mat3 transformOriented(vec3 zDir)
{
	vec3 yDir;

	if(abs(zDir.y) > 0.5)
	{
		yDir = vec3(0.0, 0.0, 1.0);
	}
	else
	{
		yDir = vec3(0.0, 1.0, 0.0);
	}

	vec3 xDir = normalize(cross(zDir, yDir));
	yDir = normalize(cross(zDir, xDir));

	return mat3(xDir, yDir, zDir);
}

vec3 scatterDirection(vec3 originalDir, float scatterCos)
{
	// scatter in 2D by scatterCos
	vec2 flatDir = vec2(scatterCos, sqrt(1.0 - scatterCos*scatterCos));

	// random rotation
	float rot = randHybridTaus() * 2.0 * M_PI;

	// scattered dir along z
	vec3 dir = vec3(sin(rot) * flatDir.y, cos(rot) * flatDir.y, flatDir.x);

	// return dir oriented along originalDir
	return transformOriented(originalDir) * dir;
}

struct PhotonState
{
	vec3 posMeters;
	vec3 posWorld;
	vec3 dirMeters;
	float flux;
};

void woodcockStepVirtual(inout PhotonState photon, in float maxExtCoeff,
                         out float currentExtCoeff, out float distMeters)
{
	distMeters = -log(1.0 - randHybridTaus()) / maxExtCoeff;
	photon.posMeters += photon.dirMeters * distMeters;
	photon.posWorld = metersToWorldPos(photon.posMeters);
	currentExtCoeff = sampleExtinctionCoefficient(photon.posWorld);
}

bool woodcockStepComplete(inout PhotonState photon, in float maxExtCoeff,
                          out float currentExtCoeff, out float distMeters)
{
	woodcockStepVirtual(photon, maxExtCoeff, currentExtCoeff, distMeters);

	if(currentExtCoeff > 0.0)
	{
		return randHybridTaus() < currentExtCoeff / maxExtCoeff;
	}
	else
	{
		return false;
	}
}

#ifdef DEPOSIT_PHOTON_POSTPONED
ivec3 currentPhotonTexel = ivec3(-1, -1, -1);
uint currentPhotonTexelFlux = 0;
int currentPhotonTexelCosine = 0;
uint currentPhotonTexelCount = 0;

void flushPhotonTexel()
{
    if(currentPhotonTexel.x >= 0 && currentPhotonTexel.y >= 0 && currentPhotonTexel.z >= 0)
    {
        imageAtomicAdd(photonFluxMapOut, currentPhotonTexel, currentPhotonTexelFlux);
        imageAtomicAdd(photonCosineMapOut, currentPhotonTexel, currentPhotonTexelCosine);
        imageAtomicAdd(photonCountMapOut, currentPhotonTexel, currentPhotonTexelCount);
    }
    currentPhotonTexelFlux = 0;
    currentPhotonTexelCosine = 0;
    currentPhotonTexelCount = 0;
}
#endif


void depositPhoton(PhotonState photon, float stepDistMeters, ivec3 size)
{
	vec3 dirWorld = normalize(metersToWorldPos(photon.posMeters + photon.dirMeters) - photon.posWorld);
	float photonCosine = dot(volumeLightingDirection, dirWorld);
	float cosineValue = photonCosine * PHOTON_MAP_NORMALIZATION_VALUE;

	vec3 texCoordOut = getLightingTextureCoords(dataExtent, photon.posWorld);
	ivec3 coord = ivec3(texCoordOut * vec3(size));

	float fluxValue = photon.flux * PHOTON_MAP_NORMALIZATION_VALUE * stepDistMeters;

#ifdef DEPOSIT_PHOTON_POSTPONED
	if (coord != currentPhotonTexel)
	{
	    flushPhotonTexel();
	    currentPhotonTexel = coord;
	}
    currentPhotonTexelFlux += uint(fluxValue);
    currentPhotonTexelCosine += int(cosineValue);
    currentPhotonTexelCount += uint(1);
#else
    imageAtomicAdd(photonFluxMapOut, coord, uint(fluxValue));
    imageAtomicAdd(photonCosineMapOut, coord, int(cosineValue));
    imageAtomicAdd(photonCountMapOut, coord, uint(1));
#endif
}

void trackRestPhotonBeam(in PhotonState photon, in float maxExtCoeff, in ivec3 mapSize, in vec3 boundsMin, in vec3 boundsMax)
{
	while(positionInBounds(boundsMin, boundsMax, photon.posWorld))
	{
		float distMeters;
		float extCoeff;
		woodcockStepVirtual(photon, maxExtCoeff, extCoeff, distMeters);

		if(extCoeff >= 0.0)
		{
			photon.flux *= transmittance(distMeters, extCoeff);
		}

        depositPhoton(photon, maxExtCoeff, mapSize);

		if(photon.flux < PHOTON_FLUX_THRESHOLD)
			break;
	}
}

bool pointInBounds(vec3 point, vec3 boundsMin, vec3 boundsMax)
{
	return all(greaterThanEqual(point, boundsMin))
           && all(lessThanEqual(point, boundsMax));
}

shader CSPhoton()
{
	randSetSeed(gl_GlobalInvocationID.x);

	ivec3 mapSize = imageSize(photonFluxMapOut);

	vec3 boundsMin = getLightingWorldBoundsMin(dataExtent);
	vec3 boundsMax = getLightingWorldBoundsMax(dataExtent);
	vec3 boundsExtent = boundsMax - boundsMin;

	PhotonState photon;
	photon.posWorld = randomPhotonEntry();
	photon.posWorld += volumeLightingDirection * 0.0001; // tiny offset, so the photon is in the volume
	photon.posMeters = worldPosToMeters(photon.posWorld);
	photon.dirMeters = normalize(worldPosToMeters(photon.posWorld + volumeLightingDirection) - photon.posMeters);
	photon.flux = 1.0;

	while(positionInBounds(boundsMin, boundsMax, photon.posWorld))
	{
		float distMeters;
		float extCoeff;
		bool realScatterEvent = woodcockStepComplete(photon, volumeLightingMaxExtinctionCoeff,
		                                             extCoeff, distMeters);

		if(extCoeff >= 0.0)
		{
			photon.flux *= transmittance(distMeters, extCoeff);
		}

        depositPhoton(photon, distMeters, mapSize);

		if(realScatterEvent)
		{
            trackRestPhotonBeam(photon, volumeLightingMaxExtinctionCoeff, mapSize, boundsMin, boundsMax);

            photon.flux = 1.0;
			float scatterCos = sampleHenyeyGreenstein(randHybridTaus(),
				volumeLightingHenyeyGreensteinG);
			photon.dirMeters = normalize( // TODO: already normalized?
			    scatterDirection(photon.dirMeters, scatterCos));
		}

		if(photon.flux < PHOTON_FLUX_THRESHOLD)
			break;
	}

#ifdef DEPOSIT_PHOTON_POSTPONED
	flushPhotonTexel();
#endif
}

/*****************************************************************************/

shader CSPhotonAccumulate()
{
	ivec3 texelCoord = ivec3(gl_GlobalInvocationID.xyz);

    uint fluxValue = texelFetch(photonFluxMap, texelCoord, 0).r;
    float flux = float(fluxValue) / PHOTON_MAP_NORMALIZATION_VALUE;

    int cosineValue = texelFetch(photonCosineMap, texelCoord, 0).r;
    float cosine = float(cosineValue) / PHOTON_MAP_NORMALIZATION_VALUE;

    uint photonCount = texelFetch(photonCountMap, texelCoord, 0).r;
    if(photonCount > 0)
    {
        cosine /= float(photonCount);
    }

    imageStore(photonCacheCompleteOut, texelCoord, vec4(flux, cosine, 0.0, 0.0));
}


/*****************************************************************************/

layout(binding = 0) uniform atomic_uint maxExtinctionCoeffCounter;

uniform float maxExtinctionCoeffScale;

shader CSPhotonMaxExtinctionCoeff()
{
	ivec3 texelCoord = ivec3(gl_GlobalInvocationID.xyz);
	ivec3 size = textureSize(dataVolumeLWC, 0);
	vec3 pos = vec3(texelCoord) / vec3(size);
	vec3 worldPos = getLightingWorldPosition(dataExtentLWC, pos);

    float extCoeff = sampleExtinctionCoefficient(worldPos);
    float scaledValue = extCoeff * maxExtinctionCoeffScale;

    uint v;
    if (scaledValue >= 4294967295.0)
    {
        v = 4294967295;
    }
    else
    {
        v = uint(scaledValue);
    }

    atomicCounterMax(maxExtinctionCoeffCounter, v);
}


/*****************************************************************************
 ***                             PROGRAMS
 *****************************************************************************/

program SimpleLighting
{
    cs(430)=CSSimpleLighting()
		: in(local_size_x = 1, local_size_y = 1, local_size_z = 1);
};

program Photon
{
	cs(430)=CSPhoton()
		: in(local_size_x = 1, local_size_y = 1, local_size_z = 1);
};

program PhotonAccumulate
{
	cs(430)=CSPhotonAccumulate()
		: in(local_size_x = 1, local_size_y = 1, local_size_z = 1);
};

program PhotonMaxExtinctionCoeff
{
    cs(460)=CSPhotonMaxExtinctionCoeff()
        : in(local_size_x = 1, local_size_y = 1, local_size_z = 1);
};
