/******************************************************************************
**
**  This file is part of Met.3D -- a research environment for the
**  three-dimensional visual exploration of numerical ensemble weather
**  prediction data.
**
**  Copyright 2017 Florian Maerkl
**
**  Computer Graphics and Visualization Group
**  Technische Universitaet Muenchen, Garching, Germany
**
**  Met.3D is free software: you can redistribute it and/or modify
**  it under the terms of the GNU General Public License as published by
**  the Free Software Foundation, either version 3 of the License, or
**  (at your option) any later version.
**
**  Met.3D is distributed in the hope that it will be useful,
**  but WITHOUT ANY WARRANTY; without even the implied warranty of
**  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
**  GNU General Public License for more details.
**
**  You should have received a copy of the GNU General Public License
**  along with Met.3D.  If not, see <http://www.gnu.org/licenses/>.
**
******************************************************************************/

#define VARIABLE_MODE_RAW_VARIABLE  0
#define VARIABLE_MODE_CLOUD         1


uniform float effectiveRadiusLWCum; // = 10.0;
uniform float effectiveRadiusIWCum; // = 25.0;

uniform float worldZMeterScale;

uniform bool earthCosineEnabled;
uniform float spaceScale;

uniform bool uniformViewScale;
uniform float spaceScaleView;


#define EARTH_PERIMETER_METERS 40030173.0

vec2 lonLatToMeters(vec2 lonLat)
{
    vec2 ret = vec2(
        (lonLat.x / 360.0) * EARTH_PERIMETER_METERS,
        (lonLat.y / 360.0) * EARTH_PERIMETER_METERS
    ) * spaceScale;

    if(earthCosineEnabled)
    {
        float latRad = lonLat.y * M_PI / 180.0;
        ret.x *= cos(latRad);
    }

    return ret;
}

vec2 metersToLonLat(vec2 meters)
{
    float latNorm = meters.y / EARTH_PERIMETER_METERS;
    vec2 ret = vec2(
        (meters.x / EARTH_PERIMETER_METERS) * 360.0,
        latNorm * 360.0
    ) / spaceScale;

    if (earthCosineEnabled)
    {
        float latRad = latNorm * M_PI * 2.0;
        ret.x /= cos(latRad);
    }

    return ret;
}

float worldZToMeters(float z)
{
    return z * worldZMeterScale * spaceScale;
}

float metersToWorldZ(float meters)
{
    return meters / (worldZMeterScale * spaceScale);
}

vec3 worldPosToMeters(vec3 pos)
{
    return vec3(lonLatToMeters(pos.xy), worldZToMeters(pos.z));
}

vec3 metersToWorldPos(vec3 pos)
{
    return vec3(metersToLonLat(pos.xy), metersToWorldZ(pos.z));
}



vec3 worldPosToMetersView(vec3 pos)
{
    if(uniformViewScale)
    {
        return pos * worldZMeterScale * spaceScale * spaceScaleView;
    }
    else
    {
        return vec3(lonLatToMeters(pos.xy), worldZToMeters(pos.z)) * spaceScaleView;
    }
}

vec3 metersToWorldPosView(vec3 pos)
{
    if(uniformViewScale)
    {
        return pos / (worldZMeterScale * spaceScale * spaceScaleView);
    }
    else
    {
        return vec3(metersToLonLat(pos.xy), metersToWorldZ(pos.z)) / spaceScaleView;
    }
}





vec3 getLightingWorldBoundsMin(in DataVolumeExtent dve)
{
	return vec3(
	    volumeTopNWCrnr.x,
		volumeBottomSECrnr.y,
		volumeBottomSECrnr.z
	);
}

vec3 getLightingWorldBoundsMax(in DataVolumeExtent dve)
{
	return vec3(
	    volumeBottomSECrnr.x,
		volumeTopNWCrnr.y,
		volumeTopNWCrnr.z
	);
}

float getLightingTexelVolumeInMetersAtWorldPos(ivec3 size, vec3 pos)
{
    vec3 boundsMin = getLightingWorldBoundsMin(dataExtent);
    vec3 boundsMax = getLightingWorldBoundsMax(dataExtent);

    vec3 texelExtentMeter;
    if(earthCosineEnabled)
    {
        vec3 extentEquator = worldPosToMeters(vec3(boundsMax.x, 0.0, boundsMax.z))
                        -  worldPosToMeters(vec3(boundsMin.x, 0.0, boundsMin.z));
        float extentY = worldPosToMeters(vec3(0.0, boundsMax.y, 0.0)).y
                    -  worldPosToMeters(vec3(0.0, boundsMin.y, 0.0)).y;
        vec3 totalExtent = vec3(extentEquator.x, extentY, extentEquator.z);
        texelExtentMeter = totalExtent / vec3(size);
        texelExtentMeter.x *= cos(pos.y * M_PI / 180.0);

        /*vec3 texelHalfExtentWorld = 0.5 * (boundsMax - boundsMin) / vec3(size);
        vec3 texelExtentMeter = worldPosToMeters(pos + texelHalfExtentWorld)
                    - worldPosToMeters(pos - texelHalfExtentWorld);*/
    }
    else
    {
        vec3 meterExtent = worldPosToMeters(boundsMax)
            - worldPosToMeters(boundsMin);
        texelExtentMeter = meterExtent / vec3(size);
    }

    return texelExtentMeter.x * texelExtentMeter.y * texelExtentMeter.z;
}

vec3 getLightingTextureCoordsFromBounds(in vec3 boundsMin, in vec3 boundsMax, in vec3 worldPos)
{
	vec3 extent = boundsMax - boundsMin;

	if(extent.x < 0.0001 || extent.y < 0.0001 || extent.z < 0.0001)
		return vec3(0.0);

	vec3 texCoord = (worldPos - boundsMin);
	texCoord.x = mod(texCoord.x, 360.0);
	texCoord /= extent;
	texCoord.z = 1.0 - texCoord.z;
	return texCoord;
}

bool positionInBounds(in vec3 boundsMin, in vec3 boundsMax, in vec3 pos)
{
    return all(greaterThanEqual(pos, boundsMin))
        && all(lessThanEqual(pos, boundsMax));
}

vec3 getLightingTextureCoords(in DataVolumeExtent dve, in vec3 worldPos)
{
	return getLightingTextureCoordsFromBounds(
		getLightingWorldBoundsMin(dve),
		getLightingWorldBoundsMax(dve),
		worldPos
	);
}

vec3 getLightingWorldPositionFromBounds(in vec3 boundsMin, in vec3 boundsMax, in vec3 texCoord)
{
	texCoord.z = 1.0 - texCoord.z;
	return boundsMin + (boundsMax - boundsMin) * texCoord;
}

vec3 getLightingWorldPosition(in DataVolumeExtent dve, in vec3 texCoord)
{
	return getLightingWorldPositionFromBounds(
		getLightingWorldBoundsMin(dve),
		getLightingWorldBoundsMax(dve),
		texCoord
	);
}

float sampleLightingVolumeAtPos(in sampler3D sampler,
                               in DataVolumeExtent dve,
                               in vec3 pos)
{
    vec3 texCoords = getLightingTextureCoords(dve, pos);
    if ((texCoords.p < 0.0) || (texCoords.p > 1.)) return 1.0; //M_MISSING_VALUE;
    return texture(sampler, texCoords).r;
}

vec2 samplePhotonMapAtPos(in sampler3D sampler,
                          in DataVolumeExtent dve,
                          in vec3 pos)
{
    vec3 texCoords = getLightingTextureCoords(dve, pos);
    if ((texCoords.p < 0.0) || (texCoords.p > 1.)) return vec2(0.0, 0.0); //M_MISSING_VALUE;
    return texture(sampler, texCoords).rg;
}

float computeLWCExtinctionCoefficient(float lwcKgPerM3)
{
    // Hu & Stamnes 1992 Parameterization for LWC
    //return 3.0 * lwc / 2.0 / effectiveRadiusLWCum * 1000000.0 * pow(10, -6) / 1.2;  // global mean effective radius 10 micron

    float effectiveRadiusMeter = effectiveRadiusLWCum / 1000000.0;
    float densityOfWaterKgPerM3 = 1000.0;
    return 1.5 * lwcKgPerM3 / (densityOfWaterKgPerM3 * effectiveRadiusMeter);
}

float computeIWCExtinctionCoefficient(float iwcKgPerM3)
{
    // Fu 1996 Paramerterization for IWC
    //return 4.0 * sqrt(3.0) * iwc / 3.0 / effectiveRadiusIWCum * 1000000.0; // global effective ice crystal size 25 micron

    float effectiveRadiusMeter = effectiveRadiusIWCum / 1000000.0;
    float densityOfIceKgPerM3 = 917.0;
    return (4.0 * sqrt(3.0) / 3.0) * iwcKgPerM3 / (densityOfIceKgPerM3 * effectiveRadiusMeter);
}

float sampleExtinctionCoefficient(in vec3 pos)
{
    if(variableMode == VARIABLE_MODE_RAW_VARIABLE)
    {
        float scalar = sampleDataAtPos(pos);
        float t = (scalar - dataExtent.tfMinimum)
            / (dataExtent.tfMaximum - dataExtent.tfMinimum);
        return texture(transferFunction, t).a;
    }
    else // VARIABLE_MODE_CLOUD
    {
        float p = exp(pos.z / pToWorldZParams.y + pToWorldZParams.x);
        float temperatureCelsius = 20.0;

        float gasConstant = 287.058;
        float densityOfAir = p * 100.0 / (gasConstant * (temperatureCelsius + 273.15));

        vec2 wc = sampleCloudWaterContentAtPos(pos) * densityOfAir;
        return computeLWCExtinctionCoefficient(wc.x)
            + computeIWCExtinctionCoefficient(wc.y);
    }
}


float calculateHenyeyGreenstein(float theta, float g)
{
	return (1.0 - g*g) /
		(4.0 * M_PI * pow(1.0 + g*g - 2*g*cos(theta), 1.5));
}

float calculateHenyeyGreensteinCos(float u, float g)
{
	return (1.0 - g*g) /
		(4.0 * M_PI * pow(1.0 + g*g - 2*g*u, 1.5));
}

/**
 * "Inverse" of calculateHenyeyGreensteinCos,
 * returns cosine values distributed according * to the
 * Henyey Greenstein function with g for a random value r in [0, 1]
 *
 * see https://www.astro.umd.edu/~jph/HG_note.pdf (7)
 */
float sampleHenyeyGreenstein(float r, float g)
{
    if (abs(g) < 0.00001)
    {
        return r;
    }

	float a = (1.0 - g*g) / (1.0 + g * (2.0*r - 1.0));
	return (1.0 / (2.0 * g)) * (1.0 + g*g - a*a);
}


float opticalThickness(float dist, float exctinctionCoeff)
{
    return dist * exctinctionCoeff;
}

float transmittance(float dist, float extinctionCoeff)
{
    return exp(-opticalThickness(dist, extinctionCoeff));
}